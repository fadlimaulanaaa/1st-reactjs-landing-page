import {Navbar, Container, Nav, NavbarBrand, NavDropdown} from "react-bootstrap"
import {useEffect, useState} from "react"
import axios from "axios"

const NavigationBar = () => {
    const [navMovie, setNav] = useState ([])
    const [navTV, setTV] = useState ([])
    useEffect (()=> {
        axios.get(`${process.env.REACT_APP_BASE_URL}/genre/movie/list`, {
            params: {
                api_key: process.env.REACT_APP_AUTH
            }
        }).then((response) => {
            setNav(response.data.genres)
        })
    })
    useEffect (() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/genre/tv/list`, {
            params: {
                api_key: process.env.REACT_APP_AUTH
            }
        }).then((response)=> {
            setTV(response.data.genres)
        })
    })

    return (
      <div >
        <Navbar variant="dark" fixed="top" className="bg-dark">
            
            <Container >
                <Navbar.Brand href="/">Filmin</Navbar.Brand>
                <Nav>
                    <Nav.Link href="#trending">TRENDING</Nav.Link>
                    <Nav.Link href="#superhero">SUPERHERO</Nav.Link>
                    <NavDropdown title="MOVIE" id="basic-nav-dropdown" >
                    {navMovie.map((result, index)=> {
                        return (
                            <NavDropdown.Item href="#action/3.1" key={index}>
                                {result.name}
                            </NavDropdown.Item>
                            )
                        })}
                    </NavDropdown>
                    <NavDropdown title="TV" id="basic-nav-dropdown" >
                    {navTV.map((result, index)=> {
                        return (
                            <NavDropdown.Item href="#action/3.1" key={index}>
                                {result.name}
                            </NavDropdown.Item>
                            )
                        })}
                    </NavDropdown>
                </Nav>
            </Container>
        </Navbar>
        </div>
    )
}

export default NavigationBar