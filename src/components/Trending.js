import {useEffect, useState} from "react"
import { Container, Row, Col, Image, Card } from 'react-bootstrap';
import bg2Image from "../assets/bg2.jpeg"
import axios from "axios"

const Trending = () => {
  const [movies, setMovies] = useState ([])
  useEffect (() => {
    axios.get(`${process.env.REACT_APP_BASE_URL}/discover/movie`, {
      params: {
        api_key: process.env.REACT_APP_AUTH
      }
    }).then((response) => {
      setMovies(response.data.results)
    })
  }, [])

    return (
        <div>
        <Container>
          <br/>
          <br/>
          <h1 className='text-white'>DISCOVER</h1>
            <Row>
              {movies.map((result, index) => {
                return(
                  <Col md={4} className='movieWrapper' id="trending" key ={index} >
                  <Card className="bg-dark text-white movieImage">
                    <Image src={`${process.env.REACT_APP_IMG_URL}/${result.poster_path}`} alt="Test" className='images'/>
                      <div className='p-2 m-1'>
                        <Card.Title className='text-center'>
                          {result.title}
                        </Card.Title>
                        <Card.Text className='text-left'>
                          {result.overview}
                        </Card.Text><Card.Text className='text-left'>
                          {result.release_date}
                        </Card.Text>
                    </div>
                  </Card>
                </Col>
                )
              })}
                
            </Row>
        </Container>
        </div>
    )
    
}

export default Trending