import { Container, Row, Col, Image, Card } from 'react-bootstrap';
import bg2Image from "../assets/bg2.jpeg";
import {useEffect, useState} from "react"
import axios from "axios"

const Superhero = () => {
  const [tv, setTv] = useState ([])
  useEffect (() => {
    axios.get(`${process.env.REACT_APP_BASE_URL}/discover/tv`,{
      params: {
        api_key: process.env.REACT_APP_AUTH
      }
      
    }).then((response)=> {
      setTv(response.data.results)
    })
  }, [])
    return (
        <div>
        <Container>
          <br/>
          <br/>
          <h1 className='text-white'>TV DISCOVER</h1>
            <Row>
              {tv.map((result, index)=> {
                return (
                  <Col md={4} className='movieWrapper' id="superhero" key={index}>
                  <Card className="bg-dark text-white movieImage">
                    <Image src={`${process.env.REACT_APP_IMG_URL}/${result.poster_path}`} alt="bg2 Movies" className='images'/>
                      <div className='p-2 m-1'>
                        <Card.Title className='text-center' >
                          {result.name}
                        </Card.Title>
                        <Card.Text className='text-left'>
                          This is a wider card with supporting text below as 
                        </Card.Text>
                        <Card.Text>
                          Last updated 3 mins ago
                        </Card.Text>
                    </div>
                  </Card>
                </Col>
                )
              })}
                
            </Row>
        </Container>
        </div>
    )
    
}

export default Superhero